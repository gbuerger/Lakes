addpath fun

alpha = 0.05 ;

A = importdata('inp.csv', ';', 1) ;
A.t = datenum(A.textdata(2:end,1), 'yyyy-mm-dd') ;
A.V = A.textdata(1,2:end) ;
A.n = length(A.V) ;


[coeff, se, EstCoeffCov] = arrayfun(@(j) fgls(A.t, A.data(:,j), 'ARLags', 1, 'Display', 'final', 'intercept', true), 1:A.n, 'UniformOutput',false) ;
coeff = cell2mat(coeff) ;
se = cell2mat(se) ;
EstCoeffCov = cell2mat(EstCoeffCov) ;

plot(coeff(2,:))
ci = [coeff-se ; coeff+se]' ;
plot(ci(:,[2 4])) ;
sum(ci(:,2) > 0)

j = 5 ;
t = coeff(1,j) + A.t * coeff(2,j) ;
plot(A.t, A.data(:,j), A.t, t) ;
plot(A.t, A.data(:,j) - t) ;

ac = arrayfun(@(j) autocorr(A.data(:,j) - coeff(1,j) + A.t * coeff(2,j), 'NumLags', 1), 1:A.n, 'UniformOutput',false) ;
ac = cell2mat(ac)' ;
plot(ac(:,2)) ;

