
pkg load tsa statistics tablicious signal
addpath fun

alpha = 0.05 ;
global MODE

A = read_data("inp.csv") ;

MDL = {"KS" "CO" "HL" "WS" "Y"} ;
MODE = "R" ;
if exist(rfile = sprintf("RES.%s.ob", MODE)) == 2
   load(rfile)
else
   for mdl = MDL
      mdl = mdl{:} ;
      RES.(mdl) = arrayfun(@(j) pw(A.t, A.data(:,j), mdl, Tfun, PX, MODE), 1:A.n) ;
      eval(sprintf("%s = [RES.%s.pv]' < alpha ;", mdl, mdl)) ;
   endfor
   save(rfile, "RES", MDL{:}) ;
endif

STN = A.V' ;
eval(sprintf("tab = table(STN, %s, %s, %s, %s, %s) ;", MDL{:})) ;
prettyprint (tab)
eval(sprintf("disp(sum([%s %s %s %s, %s])) ;", MDL{:})) ;

for i = 1 : length(MDL)
   mi = MDL{i} ;
   BETA(:,:,i) = reshape([RES.(mi).beta], 2, A.n) ;
   RHO(:,i) = [RES.(mi).rho] ;
   PV(:,i) = [RES.(mi).pv] ;
   RE(:,i) = [RES.(mi).re] ;
   for j = 1:i
      mj = MDL{j} ;
      PHI(j,i) = corr(double(PV(:,i) <  alpha), double(PV(:,j) <  alpha)) ;
      TP(j,i,:) = PV(:,i) <  alpha & PV(:,j) <  alpha ;
      FN(j,i,:) = PV(:,i) >= alpha & PV(:,j) <  alpha ;
      FP(j,i,:) = PV(:,i) <  alpha & PV(:,j) >= alpha ;
      TN(j,i,:) = PV(:,i) >= alpha & PV(:,j) >= alpha ;
   endfor
endfor

plot(BETA(2,:,:)) ;
plot(RHO) ;
plot(RE) ;
legend(MDL{:}) ;

[i j] = deal(5, 2) ;
ref.name = MDL{i} ; sim.name = MDL{j} ;
eval(sprintf("%s = {\"yes\"; \"no\"};", sim.name)) ;
Trend = [sum(TP(j,i,:)) ; sum(FN(j,i,:))] ;
NoTrend = [sum(FP(j,i,:)) ; sum(TN(j,i,:))] ;
eval(sprintf("tab = table(%s, Trend, NoTrend) ;", sim.name)) ;
prettyprint (tab)


set(0, "defaultaxesfontname", "Liberation Sans", "defaultaxesfontsize", 14) ;
set(0, "defaulttextfontname", "Linux Biolinum", "defaulttextfontsize", 14, "defaultlinelinewidth", 2) ;
load("col.ot") ;

## beta plot
figure(1, "position", [0.5   0.6   0.5   0.4]) ;
x = 1 : A.n ; c = 12 ; sz = 80 ;
clf ; hold on ; clear h ;
h0 = plot([1 A.n], [0 0], "k--") ;
for i = 1 : length(MDL)
   I = PV(:,i) <  alpha ;
   h(i,1) = scatter(x(~I), c*BETA(2,~I,i), sz, col(i,:)) ;
   h(i,2) = scatter(x(I), c*BETA(2,I,i), sz, col(i,:), "filled") ;
endfor
ylabel("trend  [cm/y]") ;
set(gca, "xtick", x, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.004, xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend(h(:,2), MDL, "box", "on", "location", "southeast")
mkdir("nc") ;
print(sprintf("nc/beta.svg")) ;

## trend plot
figure(2) ;
clf ; clear h ; j = 28 ;
plot(A.t, [A.data(:,j)]) ;
h = plot(A.t, A.data(:,j), A.t, [ones(length(A.t), 1) A.t] * BETA(:,j,:)) ;
datetick
xlabel("year") ; ylabel("level  [cm]") ;
set(h(1), "color", [0 0 0]) ;
arrayfun(@(j) set(h(j), "color", col(j-1,:)), 2:length(MDL)+1) ;
set(gca, "box", "off") ;
title(sprintf("%s", A.V{j})) ;
legend(h(2:end), MDL, "box", "off", "location", "southwest")
print(sprintf("nc/%s.svg", A.V{j})) ;

## residual error plot
figure(1) ; clf ;
x = 1 : A.n ;
rc = cor_crit(length(A.t), 0.05)
clf ; hold on ;
fill([0 A.n+1 A.n+1 0], [-rc -rc rc rc], 0.7*[1 1 1], "edgecolor", [1 1 1], "facealpha", 0.3) ;
scatter(x, mean(RE, 2), sz, "k", "filled") ;
ylabel("AR(1) of PW residual") ;
set(gca, "xtick", x, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.05, xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend({"RE = 0 (5%)" "RE"}, "box", "on", "location", "southeast")
mkdir("nc") ;
print(sprintf("nc/RE.svg")) ;

## pacf for station
figure(2) ;
lag = 20 ; j = 10 ;
[a cl] = pacf(A.data(:,j), lag) ;
clf ; hold on ;
fill([0 lag lag 0], [cl(1) cl(1) cl(2) cl(2)], 0.7*[1 1 1], "edgecolor", [1 1 1], "edgealpha", 0, "facealpha", 0.3) ;
h = stem((0:lag)', a, "linewidth", 1, "markersize", 6, "markerfacecolor", "b", "markeredgecolor", "none")
xlabel("lag  [m]") ; ylabel("part. autocorrelation") ;
title(sprintf("%s", A.V{j})) ;
print(sprintf("nc/%s.pac.svg", A.V{j})) ;

xlim([]) ;

## pac plot
figure(1) ;
[pcor, cl] = arrayfun(@(i) pacf(A.data(:,i), 20), 1:A.n, "UniformOutput", false) ;
pcor = cell2mat(pcor)' ; cl = cl{1} ;

clf ; hold on ;
fill([0 A.n+1 A.n+1 0], [cl(1) cl(1) cl(2) cl(2)], 0.7*[1 1 1], "edgecolor", [1 1 1], "edgealpha", 0, "facealpha", 0.3) ;
hb = bar(pcor(:,2:3)) ; 
set (hb(1), "facecolor", [0 0 0.7], "edgecolor", [0 0 0.7])
set (hb(2), "facecolor", [0 0.7 0], "edgecolor", [0 0.7 0])
ylabel("part. autocorrelation") ;
set(gca, "xtick", 1:A.n, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.02, xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend(hb, {"lag = 1" "lag = 2"}, "box", "on", "location", "southeast")
mkdir("nc") ;
print(sprintf("nc/pac.svg")) ;
