
set(0, "defaultaxesfontsize", 16, "defaulttextfontsize", 16) ;

pkg load tsa statistics tablicious signal parallel optim
cd ~/Lakes
addpath([pwd "/fun"]) ;
[~, ~] = mkdir("nc") ; mkdir(Tdir = tempname) ;

alpha = 0.05 ;
global MODE
PWx = 1 ; ANOM = true ;
opts = optimset() ;
opts.Display = "off" ;
opts.MaxIter = 100 ;
opts.Meth = "yule" ;
opts.TolX = 1e-3 ;

MDL = {"KS" "CO" "Y" "Ys" "HL" "ML" "H09"} ;
Tfun = {@ols_est, @ThS}{1} ;
# PWx = 3 ;
MODE = "R" ;

A = read_data("inp.csv") ;
A.adata = anom(A.m, A.data) ;

## remove annual cycle
if ANOM
   AFX = "_anom" ;
   A.x = A.adata ;
else
   AFX = "" ;
   A.x = A.data ;
endif

## estimate ARMA:
A.X = onex(A.m) ;
[beta sigma] = pararrayfun(nproc, @(j) ols(A.x(:,j), A.X), 1:A.n, "UniformOutput", false) ;
A.beta = cell2mat(beta) ; A.sigma = cell2mat(sigma) ;

Y = detrend(A.x) ; # detrend
for p = 1 : 3
   sa = sprintf("ac%d", p) ; sv = sprintf("av%d", p) ;
   [ac av] = arrayfun(@(j) ARfit(Y(:,j), p), 1:A.n, "UniformOutput", false) ;
   A.(sa) = cell2mat(ac')' ; A.(sv) = cell2mat(av) ;
endfor

## ADF test (needs ML)
for j = 1 : A.n
    y = A.adata(:,j) ;
    [h(j),pValue(j),stat,cValue,reg] = adftest(y, 'lags', 3, 'model', 'AR', 'alpha', 0.05) ;
end
##for j = 1 : A.n
##   [beta, se, t, tsig, resid, rss, sigma] = adfreg (A.adata(:,1), dlags=10) ;
##   h(j) = all(tsig(2:end) < 0.05) ;
##endfor
##sum(h)


if 0

   rand_init(:, :) ;
   col = hsv(16)(10:12,:) ; # blueish
   for j = 1 : A.n
      clf ;
      A.V{j}
      [ax h1 h2] = plotyy(A.t, A.data(:,j), A.t, A.adata(:,j)) ;
      set(ax, {"ycolor"}, {[0.2 0.5 0.2] ; "k"}) ;
      set(h1, "color", [0.2 0.5 0.2]) ;
      set(h2, "color", "k") ;
      datetick(ax(1)) ; datetick(ax(2)) ;
      xlabel(ax(1), "year") ; xlabel(ax(2), "year") ; ylabel(ax(1), "water level  [cm]") ; ylabel(ax(2), "anomaly  [cm]") ;
      set(ax, "box", "off")
      title(sprintf("%s", A.V{j})) ;
      hl = legend(ax(1), {"original" "de-seasonalized"}, "location", "southwest", "box","off") ;
      hgsave(sprintf("nc/%s.og", A.V{j})) ;
      print(sprintf("nc/%s.svg", A.V{j})) ;
      
      f1 = A.X * A.beta(:,j) + ARsim(A.ac1(:,j), A.av1(j), rows(A.t)) ;
      f2 = A.X * A.beta(:,j) + ARsim(A.ac2(:,j), A.av2(j), rows(A.t)) ;
      [ax h1 h2] = plotyy(A.t, A.x(:,j), A.t, [f1 f2]) ;
      yl = cell2mat(arrayfun(@(a) ylim(a), ax, "UniformOutput", false)) ; yl = [min(yl(:,1)) max(yl(:,2))] ;
      arrayfun(@(a) ylim(a, yl), ax) ;
      set(ax, {"ycolor"}, {"k" ; col(2,:)}) ;
      set(h1, "color", "k") ;
      set(h2(1), "color", col(1,:)) ;
      set(h2(2), "color", col(3,:)) ;
      datetick(ax(1)) ; datetick(ax(2)) ;
      xlabel(ax(1), "year") ; xlabel(ax(2), "year") ; ylabel(ax(1), "level anomaly  [cm]") ; ylabel(ax(2), "level anomaly  [cm]") ;
      set(ax, "box", "off")
      title(sprintf("%s", A.V{j})) ;
      legend(ax(2), {"p=1" "p=2"}, "box","off") ;
      hgsave(sprintf("nc/%s.MC%s.og", A.V{j}, AFX)) ;
      print(sprintf("nc/%s.MC%s.svg", A.V{j}, AFX)) ;
   endfor

   j++, plot(A.x(:,j))
   disp(cell2mat(arrayfun(@(j) acorf(center(A.x(:,j)'), 2)', 1:A.n, "UniformOutput", false)))
   plot(acorf(center(A.x(:,j)'), 50))

### motivation
   ## trend on autocorrelation
   clf ;
   subplot(2, 1, 1) ;
   b = 5 ; K = 100 ;
   a = [0.7] ;
   N = round(10.^[1:0.5:3]) ;
   clear E R ; i = 0 ;
   for n = N
      e = filter(1, [1 -a], randn(n, K)) ;
      y = b*(1:n)' + e ;
      i++ ;
      E(i) = mean(arrayfun(@(k) acorf(center(e(:,k))', 1), 1 : K)) ;
      R(i) = mean(arrayfun(@(k) acorf(center(y(:,k))', 1), 1 : K)) ;
   endfor
   semilogx(N, [E ; R]) ;
   xlabel("time series length") ;
   ylabel("$\\hat\\rho$", "interpreter", "latex") ;
   set(gca, "ygrid", "on") ;
   legend({"AR(1)" "trend + AR(1)"}, "box", "off", "location", "southeast") ;
   ## autocorrelation on trend
   subplot(2, 1, 2) ; hold on ;
   n = N(3) ;
   t = onex((1 : n)') ;
   r = 0:0.1:0.9 ;
   for j = 1 : length(r)
      E = filter(1, [1, -r(j)], randn(n, K)) ;
      B = arrayfun(@(i) regress (E(:,i), t)(2), 1 : K) ;
      q(j) = quantile(B, 0.95) ;
   endfor
   plot(r([1 end]), q([1 1]), "--", "color", 0.7*[1 1 1]) ;
   plot(r, q, "k") ;
   xlabel("$\\rho$", "interpreter", "latex") ;
   ylabel("$\\mathrm{critical\\ (95\\%)\\ \\ \\hat\\beta}$", "interpreter", "latex") ;
   hgsave("nc/motiv.og") ;
   print("nc/motiv.svg") ;

endif

## Monte Carlo
CONS = ~true ;
if CONS
   K = 10 ;
   x = (A.t(1):365/12:A.t(1)+30*10000)' ;
   sfx = ".cons" ;
else
   K = 1000 ;
   x = A.m ;
   sfx = "" ;
endif
Ox = onex(x) ;
clear B R PV ;

[~, JMDL] = setdiff(MDL, {"Ys"}, "stable") ;

for pord = 1 : 3
   for PWx = 1 : 3
      if exist(mfile = sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, pord, PWx, func2str(Tfun), sfx), "file") ~= 2
	 rand_init ; clear RES
	 for j = 1 : A.n
	    if exist(sfile = sprintf("data/tmp.%d.%d%s.%02d.ob", pord, PWx, sfx, j)) == 2
	       printf("<-- %s\n", sfile) ;
	       load(sfile) ;
	    else
	       as = sprintf("ac%d", pord) ; vs = sprintf("av%d", pord) ;
	       Y = arrayfun(@(k) Ox * A.beta(:,j) + ARsim(A.(as)(:,j), A.(vs)(j), rows(Ox)), 1 : K, "UniformOutput", false) ;
	       Y = cell2mat(Y) ;
	       for mdl = MDL(JMDL)
		  mdl = mdl{:} ;
		  res.(mdl) = pararrayfun(nproc, @(i) pw(x, Y(:,i), mdl, Tfun, PWx, MODE, opts), 1 : K, "UniformOutput", false, "ChunksPerProc", 1) ;
	       endfor
	       printf("--> %s\n", sfile) ;
	       save(sfile, "res") ;
	    endif
            for mdl = MDL(JMDL)
               mdl = mdl{:} ;
               RES.(mdl)(j,:) = res.(mdl) ;
            endfor
	 endfor
	 save(mfile, "RES") ;
	 delete(glob(sprintf("data/tmp.%d.%d%s.*.ob", pord, PWx, sfx)){:}) ;
      endif
   endfor
endfor
exit

### plots

## MC experiment
set(0, "defaultaxesfontname", "Linux Biolinum", "defaultaxesfontsize", 18) ;
set(0, "defaulttextfontname", "Linux Biolinum", "defaulttextfontsize", 13, "defaultlinelinewidth", 2) ;
addpath ~/oct/nc/maxdistcolor
copts = struct("Lmin", 0.6, "Lmax", 0.9) ;
COL = maxdistcolor(ncol = length(MDL), @sRGB_to_OSAUCS, copts) ;
##load("col.ot") ;
sfx = "" ;
[~, JMDL] = setdiff(MDL, {"Ys" "H09"}, "stable") ;
PORD = [1 2] ;
jv = 5 ;
vn = {"PWx=1 rho" "PWx=1 beta" "PWx=2 ac=1" "PWx=2 ac=2" "PWx=2 ac=1_2" "PWx=2 beta"}{jv} ;
if length(PORD) > 1
   figure(1, "position", [0.7   0.4   0.3   0.6]) ;
   spo = sprintf("%d_%d", PORD) ;
else
   figure(1, "position", [0.7   0.65   0.3   0.3]) ;
   spo = sprintf("%d", PORD) ;
endif
for j = 1 : A.n
   clf ; yl = [Inf -Inf] ; k = 0 ;
   for pord = PORD
      po = pord ;
      ax(++k) = subplot(length(PORD), 1, k) ; hold on ;
      switch vn
	 case "PWx=1 rho"
	    load(sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, po, PWx=1, func2str(Tfun), sfx)) ;
	    v = cell2mat(cellfun(@(mdl) cell2mat([{[RES.(mdl){j,:}].ac}'])(:,1), MDL(JMDL), "UniformOutput", false)) ;
	    y = [A.ac1(1,j) A.ac1(1,j)] ;
	    ylbl = "$\\hat\\rho$" ; str = "autocorrelation" ;
	 case "PWx=1 beta"
	    load(sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, po, PWx=1, func2str(Tfun), sfx)) ;
	    v = cell2mat(cellfun(@(mdl) cell2mat([{[RES.(mdl){j,:}].beta}'])(:,2), MDL(JMDL), "UniformOutput", false)) ;
	    y = [A.beta(2,j) A.beta(2,j)] ;
	    ylbl = "$\\hat\\beta$" ; str = "trend" ;
	 case "PWx=2 ac=1"
	    load(sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, po, PWx=2, func2str(Tfun), sfx)) ;
	    v = cell2mat(cellfun(@(mdl) cell2mat([{[RES.(mdl){j,:}].ac}'])(:,1), MDL(JMDL), "UniformOutput", false)) ;
	    y = [A.ac2(1,j) A.ac2(1,j)] ;
	    ylbl = "$\\hat\\varphi_1$" ; str = "autocorrelation" ;
	 case "PWx=2 ac=2"
	    load(sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, po, PWx=2, func2str(Tfun), sfx)) ;
	    v = cell2mat(cellfun(@(mdl) cell2mat([{[RES.(mdl){j,:}].ac}'])(:,2), MDL(JMDL), "UniformOutput", false)) ;
	    y = [A.ac2(2,j) A.ac2(2,j)] .* (pord ~= 1) ;
	    ylbl = "$\\hat\\varphi_2$" ; str = "autocorrelation" ;
	 case "PWx=2 ac=1_2"
	    po = 2 ;
	    load(sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, po, PWx=2, func2str(Tfun), sfx)) ;
	    v = cell2mat(cellfun(@(mdl) cell2mat([{[RES.(mdl){j,:}].ac}'])(:,pord), MDL(JMDL), "UniformOutput", false)) ;
	    y = [A.ac2(pord,j) A.ac2(pord,j)] ;
	    ylbl = sprintf("$\\hat\\varphi_%d$", pord) ; str = "autocorrelation" ;
	 case "PWx=2 beta"
	    load(sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, po, PWx=2, func2str(Tfun), sfx)) ;
	    v = cell2mat(cellfun(@(mdl) cell2mat([{[RES.(mdl){j,:}].beta}'])(:,2), MDL(JMDL), "UniformOutput", false)) ;
	    y = [A.beta(2,j) A.beta(2,j)] ;
	    ylbl = "$\\hat\\beta$" ; str = "trend" ;
      endswitch
      I = abs(v) < 1e1 ;
      v = arrayfun(@(j) v(I(:,j),j), 1 : length(JMDL), "UniformOutput", false) ;
      if sum(any(~I, 2)) > 100
	 warning("too many missing data for %d", pord) ;
      endif
      hb = violin(v, "width", 0.3, "bandwidth", 0.2) ;
      delete([hb.quartile ; hb.median']) ;
      arrayfun(@(i) set(hb.violin(i), "edgecolor", COL(JMDL(i),:), "facecolor", COL(JMDL(i),:)), 1 : length(JMDL))
      set(hb.mean, "markersize", 24, "markeredgecolor", 0.*[1 1 1])
      plot([1 length(JMDL)], y, "color", 0.7*[1 1 1]) ;
      xlabel("PW model") ; ylabel(ylbl, "interpreter", "latex") ;
      set(gca, "xtick", 1:length(JMDL), "xticklabel", MDL(JMDL))
      yl(1) = min(yl(1), ylim()(1)) ; yl(2) = max(yl(2), ylim()(2)) ;
      title(sprintf("{\\itPW^{(%d)}} %s of {\\itAR(%d)} surrogates for %s", PWx, str, po, A.V{j})) ;
   endfor
#   set(ax, "ylim", yl) ;
   hgsave(sprintf("nc/MC%s.%s.%s.%s%s.og", AFX, A.V{j}, spo, vn, sfx)) ;
   print(sprintf("nc/MC%s.%s.%s.%s%s.svg", AFX, A.V{j}, spo, vn, sfx)) ;
endfor

## MEAN/SD of MC beta results
figure(1, "position", [0.5   0.6   0.5   0.4]) ;
sfx = "" ;
sfun = {@mean @std}{2} ; pord = 2 ;
col = [0 0 0.7 ; 0 0.7 0] ;
x = 1 : A.n ; sz = 60 ; mdl = "CO" ; clear beta
clf ;
for PWx = 1 : 2
   load(sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, pord, PWx, func2str(Tfun), sfx)) ;
   beta(:,PWx) = arrayfun(@(j) sfun(cell2mat([{[RES.(mdl){j,:}].beta}'])(:,2)), 1:A.n)' ;
endfor
if isequal(sfun, @mean)
   col = [0 0 0 ; col] ;
   hb = bar([A.beta(2,:)' beta]) ;
   leg = {{"obs" "\\itPW^{(1)}" "\\itPW^{(2)}"} "box" "on" "location" "southeast"} ;
else
   hb = bar(beta) ;
   leg = {{"\\itPW^{(1)}" "\\itPW^{(2)}"} "box" "on" "location" "northeast"} ;
endif
arrayfun(@(j) set (hb(j), "facecolor", col(j,:), "edgecolor", col(j,:)), 1 : length(hb)) ;
ylabel(sprintf("$\\mathsf{%s}\\ \\hat\\beta\\ \\ \\mathsf{[cm/y]}$", func2str(sfun)), "interpreter", "latex") ;

set(gca, "xtick", x, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.1*(yt(2)-yt(1)), xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
##ylim(yl) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on", "box", "off") ;
legend(hb, leg{:})
hgsave(sprintf("nc/beta_%s%s.%d.%s%s.og", func2str(sfun), AFX, pord, mdl, sfx)) ;
print(sprintf("nc/beta_%s%s.%d.%s%s.svg", func2str(sfun), AFX, pord, mdl, sfx)) ;

## MEAN/SD of MC phi results
figure(1, "position", [0.5   0.6   0.5   0.4]) ;
sfx = "" ;
sfun = {@mean @std}{2} ;
x = 1 : A.n ; sz = 60 ; mdl = "CO" ;
PWx = 1 ; fld = sprintf("ac%d", PWx) ; clear phi
for pord = [1 2]
   load(sprintf("data/MC%s.%d.%d.%s%s.ob", AFX, pord, PWx, func2str(Tfun), sfx)) ;
   phi(:,:,pord) = cell2mat(arrayfun(@(j) sfun(cell2mat([{[RES.(mdl){j,:}].ac}']))', 1:A.n, "UniformOutput", false)) ;
endfor
i = 1 ; pord = 1:2 ;
if PWx < 2
   i = 1 ;
   str = "rho" ;
else
   str = sprintf("phi_%d", i) ;
endif
clf ;
if isequal(sfun, @mean)
   yn = min([A.(fld)(i,:)' squeeze(phi(i,:,pord))](:)) ;
   yx = max([A.(fld)(i,:)' squeeze(phi(i,:,pord))](:)) ;
   col = [0 0 0 ; 0 0 0.7 ; 0 0.7 0] ;
   hb = bar([A.(fld)(i,:)' squeeze(phi(i,:,pord))]) ;
   leg = {{"obs" "\\itAR(1)" "\\itAR(2)"} "box" "on" "location" "southeast"} ;
else
   yn = min(squeeze(phi(i,:,pord))(:)) ;
   yx = max(squeeze(phi(i,:,pord))(:)) ;
   col = [0 0 0.7 ; 0 0.7 0] ;
   hb = bar(squeeze(phi(i,:,pord))) ;
   leg = {{"\\itAR(1)" "\\itAR(2)"} "box" "on" "location" "southeast"} ;
endif
arrayfun(@(j) set (hb(j), "facecolor", col(j,:), "edgecolor", col(j,:)), 1 : length(hb)) ;
ylabel(sprintf("$\\mathsf{%s}\\ \\hat\\%s$", func2str(sfun), str), "interpreter", "latex") ;
xlim([0 41]) ;
ylim([yn-0.1*(yx-yn) yx+0.1*(yx-yn)]) ;

set(gca, "xtick", x, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), ylim()(1)-0.1*(yt(2)-yt(1)), xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on", "box", "off") ;
legend(hb, leg{:})
hgsave(sprintf("nc/%s_%s%s.%d_%d.%s%s.og", str, func2str(sfun), AFX, pord, mdl, sfx)) ;
print(sprintf("nc/%s_%s%s.%d_%d.%s%s.svg", str, func2str(sfun), AFX, pord, mdl, sfx)) ;


## estimate Lake levels
if exist(rfile = sprintf("RES%s.%d.%s.%s.ob", AFX, PWx, func2str(Tfun), MODE)) == 2

   load(rfile)

else

   for mdl = MDL
      mdl = mdl{:} ;
      RES.(mdl) = arrayfun(@(j) pw(A.m, A.x(:,j), mdl, Tfun, PWx, MODE, opts), 1:A.n) ;
      eval(sprintf("%s = [RES.%s.pv]' < alpha ;", mdl, mdl)) ;
   endfor

   STN = A.V' ;
   str = repmat(", %s", 1, length(MDL)) ;
   eval(sprintf(sprintf("tab = table(STN%s) ;", str), MDL{:})) ;
   prettyprint (tab)
   str = repmat(", %s", 1, length(MDL)) ;
   eval(sprintf(sprintf("disp(sum([%s])) ;", str), MDL{:})) ;

   clear AC
   for i = 1 : length(MDL)
      mdl = MDL{i} ;
      BETA(:,:,i) = reshape([RES.(mdl).beta], 2, A.n) ;
      I = repmat({":"}, 1, PWx+1) ; I{end} = i ;
      AC(I{:}) = cell2mat({RES.(mdl).ac}') ;
      PV(:,i) = [RES.(mdl).pv] ;
      RE(:,i) = [RES.(mdl).re] ;
      DW(:,i) = [RES.(mdl).dw] ;
      for j = 1:i
	 mj = MDL{j} ;
	 PHI(j,i) = corr(double(PV(:,i) <  alpha), double(PV(:,j) <  alpha)) ;
	 TP(j,i,:) = PV(:,i) <  alpha & PV(:,j) <  alpha ;
	 FN(j,i,:) = PV(:,i) >= alpha & PV(:,j) <  alpha ;
	 FP(j,i,:) = PV(:,i) <  alpha & PV(:,j) >= alpha ;
	 TN(j,i,:) = PV(:,i) >= alpha & PV(:,j) >= alpha ;
      endfor
   endfor
   
   save(rfile, "RES", MDL{:}, "BETA", "AC", "PV", "RE", "DW") ;

endif
[~, JMDL] = setdiff(MDL, {"Ys" "KS" "H09"}, "stable") ;
printf([repmat("%s\t", 1, length(JMDL)) "\n"], MDL{JMDL})
printf([repmat("%d\t", 1, length(JMDL)) "\n"], sum(squeeze(BETA(2,:,:)) < 0 & PV < alpha)(JMDL))
printf([repmat("%d\t", 1, length(JMDL)) "\n"], sum(squeeze(BETA(2,:,:)) > 0 & PV < alpha)(JMDL))

## beta plot
figure(1, "position", [0.5   0.6   0.5   0.4]) ;
st = {"h" "o" "p" "s" "v" "^" "d"} ;
pfx = {"" "noHL"}{1} ;
load(sprintf("RES%s.%d.%s.%s.ob", AFX, PWx, func2str(Tfun), MODE)) ;
if isempty(pfx)		    # with HL
   [~, JMDL] = setdiff(MDL, {"Ys" "KS" "H09"}, "stable") ;
else
   [~, JMDL] = setdiff(MDL, {"Ys" "KS" "HL" "ML" "H09"}, "stable") ;
endif
x = 1 : A.n ; c = 12 ; sz = 80 ;
JSTN = all(squeeze(abs(c*BETA(2,:,JMDL)) < 0.1)', 1) ;
clf ; hold on ; clear h
h0 = plot([1 A.n], [0 0], "k--") ;
for i = 1 : length(MDL)
   if ~ismember(i, JMDL) continue ; endif
   I = PV(:,i) <  alpha ;
   h(i,1) = scatter(x(~I & JSTN'), c*BETA(2,~I & JSTN',i), sz, COL(i,:), st{i}) ;
   h(i,2) = scatter(x(I & JSTN'), c*BETA(2,I & JSTN',i), sz, COL(i,:), st{i}, "filled") ;
endfor
ylabel("trend  [cm/y]") ;
V = A.V ; V(~JSTN) = repmat({""}, 1, sum(~JSTN)) ;
set(gca, "xtick", x, "xticklabel", V, "xticklabelrotation", -90)
##ylim(yl) ;
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), 1.05*yt(1), xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend(h(JMDL,2), MDL(JMDL), "box", "on", "location", "southeast")
hgsave(sprintf("nc/beta%s%s.%d.%s.%s.og", AFX, pfx, PWx, func2str(Tfun), MODE)) ;
print(sprintf("nc/beta%s%s.%d.%s.%s.svg", AFX, pfx, PWx, func2str(Tfun), MODE)) ;

## trend plot
PWx = 2 ;
[~, JMDL] = setdiff(MDL, {"Ys" "KS" "H09"}, "stable") ;
j = 0 ;
++j
for j = [2 6 8]
   clf ; clear h ; hold on ;
   plot(A.t, [A.x(:,j)], "k") ;
   ylim(ylim()) ;
   set(gca, "ColorOrder", COL(JMDL,:)) ;
   beta = cell2mat(cellfun(@(mdl) (pw(A.m, A.x(:,j), mdl, Tfun, PWx, MODE, opts)).beta, MDL, "UniformOutput", false)') ;
   plot(A.t, A.X * beta(JMDL,:)') ;
   datetick ;
   xlabel("year") ; ylabel("level anomaly [cm]") ;
   set(gca, "box", "off") ;
   title(sprintf("%s", A.V{j})) ;
   legend({"OBS" MDL{JMDL}}, "box", "off", "location", "eastoutside")
   hgsave(sprintf("nc/%s%s.%d.og", A.V{j}, AFX, PWx)) ;
   print(sprintf("nc/%s%s.%d.svg", A.V{j}, AFX, PWx)) ;
endfor

## MODE = R vs. MODE = L
PWx = 1 ; JMDL = 2 ; j = 6 ;
clf ; clear h ; hold on ;
plot(A.t, [A.x(:,j)], "k") ;
##ylim(ylim()) ;
set(gca, "ColorOrder", COL(JMDL,:)) ;
betaR = cell2mat(cellfun(@(mdl) (pw(A.m, A.x(:,j), mdl, Tfun, PWx, "R", opts)).beta, MDL, "UniformOutput", false)') ;
betaL = cell2mat(cellfun(@(mdl) (pw(A.m, A.x(:,j), mdl, Tfun, PWx, "L", opts)).beta, MDL, "UniformOutput", false)') ;
plot(A.t, A.X * betaR(JMDL,:)', A.t, A.X * betaL(JMDL,:)', "-.") ;
datetick ;
xlabel("year") ; ylabel("level anomaly [cm]") ;
set(gca, "box", "off") ;
title(sprintf("%s with {\\itPW^{(%d)}} trends", A.V{j}, PWx)) ;
legend({"OBS" [(MDL{JMDL}) " (backshift)"] [(MDL{JMDL}) " (foreshift)"]}, "box", "off", "location", "north")
hgsave(sprintf("nc/%s%s.%s.%d.og", A.V{j}, AFX, MDL{JMDL}, PWx)) ;
print(sprintf("nc/%s%s.%s.%d.svg", A.V{j}, AFX, MDL{JMDL}, PWx)) ;

## check divergence, PWx = 1
n = rows(u = A.m) ;
PWx = 1 ;
j = 2 ; xl = 0.92 ;
r = linspace(xl, 0.99999, 350)' ;
clf ; hold on ;
yo = A.x(:,j) ;
fun = @(r) nthargout (4, @pw_fun, A.m, yo, r, Tfun, MODE) ;
o = fun(r)' ;
rand_init(:, ~true) ;
ys = ARsim(A.ac1(:,j), A.av1(j), rows(A.m)) ;
fun = @(r) nthargout(4, @pw_fun, A.m, ys, r, Tfun, MODE) ;
##fun = @(r) (1 - r.^2).^(-1/n) .* nthargout(4, @pw_fun, A.m, x, r, Tfun, MODE) ;
f = fun(r) ;
[ax h1 h2] = plotyy(r, o, r, f) ;
set(h1, "color", "k") ; set(h2, "color", "b") ;
set(ax(1), "ycolor", "k") ; set(ax(2), "ycolor", "b") ;
plot(ax(1), [A.ac1(j) A.ac1(j)], [ylim(ax(1))(1) ylim(ax(1))(1) + 0.2], "--k") ;
xlim([xl 1]) ;
xlabel("{\\it\\rho}") ;
arrayfun(@(i) ylabel(ax(i), "trend residual variance"), 1:2) ; 
legend([h1 h2], {sprintf("%s", A.V{j}) sprintf("%s, surrogate", A.V{j})}, "box", "off") ; 
hgsave(sprintf("nc/regerr.%d.og", PWx)) ;
print(sprintf("nc/regerr.%d.svg", PWx)) ;

## check divergence, PWx > 1
n = rows(u = A.m) ;
col = hsv(16)(10:16,:) ; # blueish
PWx = 2 ;
j = 2 ; yI = [0 3] ; q = 0.1 ;
u = A.m ; x = A.x(:,j) ;
fun = @(p1, p2) nthargout (4, @pw_fun, u, x, [p1(:) p2(:)], Tfun, MODE) ;
JMDL = [5 2] ;
P(1,:) = pw(u, x, MDL{JMDL(1)}, Tfun, PWx, MODE, opts).ac ;
P(2,:) = pw(u, x, MDL{JMDL(2)}, Tfun, PWx, MODE, opts).ac ;
X = @(r) P(1,1) + (P(2,1) - P(1,1)) .* r ;
Y = @(r) P(1,2) + (P(2,2) - P(1,2)) .* r ;
cost = arrayfun(@(r) fun(X(r), Y(r)), r = -1 : 0.1: 1.4) ;
clf ; hold on
plot(r, cost, "k") ; 
plot(0, fun(X(0), Y(0)), "o", "markeredgecolor", COL(JMDL(1),:), "markerfacecolor", COL(JMDL(1),:)) ;
plot(1, fun(X(1), Y(1)), "o", "markeredgecolor", COL(JMDL(2),:), "markerfacecolor", COL(JMDL(2),:)) ;
xlabel("\\it\\varphi_1") ;  ylabel("trend residual variance") ;
ylim([0.9*ylim()(1) 1.1*ylim()(2)]) ;
legend({"cost function" MDL{JMDL(1:2)}}, "box", "off", "location", "northwest") ; 
hgsave(sprintf("nc/regerr.%d.og", PWx)) ;
print(sprintf("nc/regerr.%d.svg", PWx)) ;


## beta plot (remaining stations)
figure(1, "position", [0.5   0.6   0.5   0.4]) ;
PWx = 2 ;
[~, JMDL] = setdiff(MDL, {"Ys" "KS"}, "stable") ;
x = 1 : A.n ; c = 12 ; sz = 80 ;
clf ; hold on ; clear h ;
h0 = plot([1 A.n], [0 0], "k--") ;
for i = 1 : length(MDL)
   if ~ismember(i, JMDL) continue ; endif
   I = PV(:,i) <  alpha ;
   h(i,1) = scatter(x(~I & ~JSTN'), c*BETA(2,~I & ~JSTN',i), sz, COL(i,:), st{i}) ;
   h(i,2) = scatter(x(I & ~JSTN'), c*BETA(2,I & ~JSTN',i), sz, COL(i,:), st{i}, "filled") ;
endfor
ylabel("trend  [cm/y]") ;
V = A.V ; V(JSTN) = repmat({""}, 1, sum(JSTN)) ;
set(gca, "xtick", x, "xticklabel", V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.004, xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
##ylim(yl) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend(h(JMDL,2), MDL(JMDL), "box", "on", "location", "southeast")
hgsave(sprintf("nc/beta.%d.%s.%s.og", PWx, func2str(Tfun), MODE)) ;
print(sprintf("nc/beta.%d.%s.%s.svg", PWx, func2str(Tfun), MODE)) ;


## non-HL-optimality of CO
figure(2) ;
JMDL = [5 2] ;
n = rows(u = A.m) ;
PWx = 2 ;
j = 2 ; yI = [-20 70] ; q = 0.6 ;
P1 = pw(u = A.m, x = A.x(:,j), MDL{JMDL(1)}, Tfun, PWx, MODE, opts).ac ;
P2 = pw(u = A.m, x = A.x(:,j), MDL{JMDL(2)}, Tfun, PWx, MODE, opts).ac ;
fun = @(p1, p2) nthargout (4, @pw_fun, u, x, [p1(:) p2(:)], Tfun, MODE) ;
l = @(x) P1(2) + (x - P1(1)) .* (P2(2) - P1(2)) ./ (P2(1) - P1(1)) ;
x = linspace(min(P1(1), P2(1))-q, max(P1(1), P2(1))+q, 50)' ;
clf ; hold on
plot(x, fun(x, l(x)), "k")
plot([P1(1) P1(1)], [0 2], "color", COL(JMDL(1),:), "linewidth", 2) ;
plot([P2(1) P2(1)], [0 2], "color", COL(JMDL(2),:), "linewidth", 2) ;
xlabel("\\it\\varphi_1") ; ylabel("regr. error") ; 
L{1} = "\$\\it\\epsilon^2({\\varphi_1,l(\\varphi_1)})\$" ;
L{2} = sprintf("%s opt;", MDL{JMDL(1)}) ;
L{3} = sprintf("%s opt;", MDL{JMDL(2)}) ;
legend(L, "box", "off", "location", "southeast", "interpreter", "latex") ;
hgsave(sprintf("nc/HL-CO.regerr.%d.og", PWx)) ;
print(sprintf("nc/HL-CO.regerr.%d.svg", PWx)) ;


## non-CO-optimality of HL
figure(2) ;
JMDL = [5 2] ;
n = rows(u = A.m) ;
PWx = 2 ; j = 0
++j , yI = [-20 70] ; q = 0.6 ;
P1 = pw(u = A.m, x = A.x(:,j), MDL{JMDL(1)}, Tfun, PWx, MODE, opts).ac ;
P2 = pw(u = A.m, x = A.x(:,j), MDL{JMDL(2)}, Tfun, PWx, MODE, opts).ac ;
fun = @(p1, p2) nthargout (5, @pw_fun, u, x, [p1(:) p2(:)], Tfun, MODE) ;
l = @(x) P1(2) + (x - P1(1)) .* (P2(2) - P1(2)) ./ (P2(1) - P1(1)) ;
x = linspace(min(P1(1), P2(1))-q, max(P1(1), P2(1))+q, 50)' ;
clf ; hold on
plot(x, fun(x, l(x)), "k")
plot([P1(1) P1(1)], [0.995 1.01], "color", COL(JMDL(1),:), "linewidth", 2) ;
plot([P2(1) P2(1)], [0.995 1.01], "color", COL(JMDL(2),:), "linewidth", 2) ;
xlabel("\\it\\varphi_1") ; ylabel("DW") ; 
L{1} = "\$\\it DW({\\varphi_1,l(\\varphi_1)})\$" ;
L{2} = sprintf("%s opt;", MDL{JMDL(1)}) ;
L{3} = sprintf("%s opt;", MDL{JMDL(2)}) ;
legend(L, "box", "off", "location", "southeast", "interpreter", "latex") ;
hgsave(sprintf("nc/%s.HL-CO.DW.%d.og", A.V{j}, PWx)) ;
print(sprintf("nc/%s.HL-CO.DW.%d.svg", A.V{j}, PWx)) ;

## the AD 2010 breakpoint
clf ;
col = maxdistcolor(ncol = length(~JSTN), @sRGB_to_OSAUCS, copts) ;
set(gca, "ColorOrder", col) ;
plot(A.t, zscore(A.x(:,~JSTN)))
datetick
xlabel("year") ; ylabel("norm. units") ;
set(gca, "box", "off") ;
hl = legend(A.V(~JSTN), "box", "off", "location", "eastoutside") ;
set(findall(hl, "-property", "fontsize"), "fontsize", 18)
hgsave(sprintf("nc/AD2010%s.og", AFX)) ;
print(sprintf("nc/AD2010%s.svg", AFX)) ;


## trend plot
reset(0) ; close(2) ;
set(0, "defaultaxesfontname", "Linux Biolinum", "defaulttextfontname", "Linux Biolinum") ;
set(0, "defaultfigureunits", "normalized", "defaultfigurepapertype", "A4") ;
set(0, "defaultfigurepaperunits", "normalized", "defaultfigurepaperpositionmode", "auto") ;
set(0, "defaultaxesfontsize", 18, "defaulttextfontsize", 13, "defaultlinelinewidth", 2) ;
##set(0, "defaultaxesfontsize", 16, "defaulttextfontsize", 16) ;
set(0, "defaultfigureposition", [0.7 0.7 0.3 0.3]) ;
set(0, "defaultaxesgridalpha", 0.3)
CCOL = [0 0 0.7 ; 0 0.7 0 ; 0.7 0 0 ; 0.9 0.9 0] ;

figure(2) ;
[~, JMDL] = setdiff(MDL, {"Ys" "KS"}, "stable") ;
PWx = 1 ;
JSTN = [2 25 28 29] ;
for j = 1 : length(JSTN)
   jj = JSTN(j) ;
   ax(j) = subplot(2, 5, {1:2 4:5 6:7 9:10}{j}) ; cla ; hold on ;
   plot(A.t, [A.x(:,jj)], "k") ;
   set(gca, "ColorOrder", COL(JMDL,:)) ;
   beta = cell2mat(cellfun(@(mdl) (pw(A.m, A.x(:,jj), mdl, Tfun, PWx, MODE, opts)).beta, MDL, "UniformOutput", false)') ;
   plot(A.t, A.X * beta(JMDL,:)') ;
   datetick ;
   xlabel("year") ; ylabel("level  [cm]") ;
   set(gca, "box", "off") ;
   title(sprintf("%s", A.V{jj})) ;
endfor
hl = legend({"OBS" MDL{JMDL}}, "box", "off", "location", "northeast")
set(hl, "position", [0.45 0.4 0.09 0.2])
hgsave(sprintf("nc/fail.%d.og", PWx)) ;
print(sprintf("nc/fail.%d.svg", PWx)) ;



[rho, f] = grids(fun, rho = [1e-9 1], 20, iter = 0) ;
plot(A.m, [zscore(A.x(:,j)) arma_rnd(A.ac(:,j), [], A.av(j), rows(A.m))]) ;
[rho, fval, exitflag] = fminsearch (@(rho) nthargout(4, @pw_fun, A.m, A.x(:,j), rho, Tfun, MODE), rho=0.5, opts) ;
[y beta re] = pw_fun(u, x, rho, Tfun, MODE) ;


## DW plot
figure(1, "position", [0.5   0.6   0.5   0.4]) ;
[~, JMDL] = setdiff(MDL, {"Ys" "KS"}, "stable") ;
JSTN = true(1, A.n) ;
##JSTN = and(cellfun(@(c) ~ismember(A.V, c), A.V([2 25 28]), "UniformOutput", false)'{:}) ;
x = 1 : A.n ; c = 1 ; sz = 80 ;
clf ; hold on ; clear h ;
##h0 = plot([1 sum(JSTN)], [0 0], "k--") ;
for i = 1 : length(MDL)
   if ~ismember(i, JMDL) continue ; endif
   h(i) = scatter(x(JSTN'), c*DW(JSTN',i), sz, COL(i,:), st{i}, "filled") ;
endfor
ylabel("DW statistic") ;
set(gca, "xtick", x, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.001, xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend(h(JMDL), MDL(JMDL), "box", "on", "location", "southeast")
hgsave(sprintf("nc/dw.%d.%s.%s.og", PWx, func2str(Tfun), MODE)) ;
print(sprintf("nc/dw.%d.%s.%s.svg", PWx, func2str(Tfun), MODE)) ;

## residual error plot
jSTAT = 1 ; jMDL = 2 ; ylbl = {"\\it\\rho" "DW"}{jSTAT} ; STAT = {"RE" "DW"}{jSTAT} ;
figure(1) ;
[~, JMDL] = setdiff(MDL, {"Ys" "KS"}, "stable") ;
x = 1 : A.n ; sz = 60 ;
rc = cor_crit(length(A.m), 0.05) ;
clf ; hold on ; clear S lg
if jSTAT == 1
   fill([0 A.n+1 A.n+1 0], [-rc -rc rc rc], 0.7*[1 1 1], "edgecolor", 0.7*[1 1 1], "edgealpha", 0.5, "facealpha", 0.5) ;
endif
for PWx = 1 : 4
   w = eval(sprintf("load(\"RES%s.%d.%s.%s.ob\").%s ;", AFX, PWx, func2str(Tfun), MODE, STAT)) ;
   S(:,PWx) = w(:,jMDL) ;
   lg{PWx} = sprintf("\\itPW^{(%d)}", PWx) ;
endfor
hb = bar(S) ; 
arrayfun(@(j) set (hb(j), "facecolor", CCOL(j,:), "edgecolor", CCOL(j,:)), 1 : length(hb)) ;
ylabel("$\\hat\\rho$", "interpreter", "latex") ;
set(gca, "xtick", x, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.02, xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
##ylim(yl) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend(hb, lg, "box", "on", "location", "northwest")
hgsave(sprintf("nc/%s%s.%s.og", STAT, AFX, MDL{jMDL})) ;
print(sprintf("nc/%s%s.%s.svg", STAT, AFX, MDL{jMDL})) ;

## pacf for station
figure(1, "position", [0.7   0.4   0.3   0.6]) ;
lag = 20 ; j = 10 ;
clf ;
subplot(2, 1, 1) ; hold on ;
[a cl] = pacf(detrend(A.data(:,j)), lag) ;
fill([0 lag lag 0], [cl(1) cl(1) cl(2) cl(2)], 0.7*[1 1 1], "edgecolor", 0.7*[1 1 1], "edgealpha", 0.3, "facealpha", 1) ;
h = stem((0:lag)', a, "linewidth", 1, "markersize", 6, "markerfacecolor", "b", "markeredgecolor", "none") ;
xlabel("lag  [m]") ; ylabel("part. autocorrelation") ;
title(sprintf("%s", A.V{j})) ;
subplot(2, 1, 2) ; hold on ;
[a cl] = pacf(detrend(A.adata(:,j)), lag) ;
fill([0 lag lag 0], [cl(1) cl(1) cl(2) cl(2)], 0.7*[1 1 1], "edgecolor", 0.7*[1 1 1], "edgealpha", 0.3, "facealpha", 1) ;
h = stem((0:lag)', a, "linewidth", 1, "markersize", 6, "markerfacecolor", "b", "markeredgecolor", "none") ;
xlabel("lag  [m]") ; ylabel("part. autocorrelation") ;
title(sprintf("%s, de-seasonalized", A.V{j})) ;

hgsave(sprintf("nc/%s.pac.og", A.V{j})) ;
print(sprintf("nc/%s.pac.svg", A.V{j})) ;

xlim([]) ;

## pac plot
figure(1, "position", [0.5   0.6   0.5   0.4]) ;
[pcor, cl] = arrayfun(@(j) pacf(detrend(A.x(:,j)), 20), 1:A.n, "UniformOutput", false) ;
pcor = cell2mat(pcor)' ; cl = cl{1} ;

clf ; hold on ;
hf = fill([0 A.n+1 A.n+1 0], [cl(1) cl(1) cl(2) cl(2)], 0.7*[1 1 1], "edgecolor", 0.7*[1 1 1], "edgealpha", 0.3, "facealpha", 1) ;
hb = bar(pcor(:,2:4)) ;
arrayfun(@(j) set (hb(j), "facecolor", CCOL(j,:), "edgecolor", CCOL(j,:)), 1 : length(hb)) ;
ylabel("part. autocorrelation") ;
set(gca, "xtick", 1:A.n, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.02, xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend([hb ; hf], {"lag 1" "lag 2" "lag 3" "CI"}, "box", "off", "location", "southeast")
hgsave(sprintf("nc/pac%s.og", AFX)) ;
print(sprintf("nc/pac%s.svg", AFX)) ;

## acf plot
figure(1, "position", [0.5   0.6   0.5   0.4]) ;

clf ;
set(gca, "colororder", lines(2)) ;
hb = bar(A.ac2') ; 
set (hb(1), "facecolor", lines(2)(1,:), "edgecolor", lines(2)(1,:))
set (hb(2), "facecolor", lines(2)(2,:), "edgecolor", lines(2)(2,:))
ylabel("AR(2) coefficients") ;
set(gca, "xtick", 1:A.n, "xticklabel", A.V, "xticklabelrotation", -90)
xtl = xticklabels ();
xt = xticks ();
yt = yticks ();
for ii = 1:numel (xtl)
  text (xt(ii), yt(1)-0.02, xtl{ii}, "rotation", 90, "horizontalalignment", "right")
endfor
xticklabels ([]);
xlim([0 41]) ;
set(gca, "position", [0.1   0.3   0.85   0.65], "xgrid", "on") ;
legend(hb, {"a_1" "a_2"}, "box", "off", "location", "northwest")
hgsave(sprintf("nc/acf%s.og", AFX)) ;
print(sprintf("nc/acf%s.svg", AFX)) ;
