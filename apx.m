addpath ~/oct/nc/maxdistcolor
copts = struct("Lmin", 0.6, "Lmax", 0.9) ;
COL = maxdistcolor(ncol = length(MDL), @sRGB_to_OSAUCS, copts) ;

reset(0) ;
set(0, "defaultaxesfontname", "Libertinus Sans", "defaultaxesfontsize", 18) ;
set(0, "defaulttextfontname", "Libertinus Sans", "defaulttextfontsize", 18, "defaultlinelinewidth", 2) ;
set(0, "defaultfigureunits", "normalized", "defaultfigurepapertype", "A4") ;
set(0, "defaultfigurepaperunits", "normalized", "defaultfigurepaperpositionmode", "auto") ;
set(0, "defaultfigureposition", [0.7 0.7 0.3 0.3]) ;
set(0, "defaultaxesgridalpha", 0.3)

## KS example
figure(1, "position", [0.7   0.4   0.3   0.6]) ;
j = 14 ;

yt = [0.6 10] ;

PWx = 1 ;
load(sprintf("RES%s.%d.%s.%s.ob", AFX, PWx, func2str(Tfun), MODE)) ;
clf ; hold on
plot(arrayfun(@(r) r.pv, RES.Y)')
plot(arrayfun(@(r) r.pv, RES.Ys)')
[arrayfun(@(r) r.pv, RES.Y)' arrayfun(@(r) r.pv, RES.Ys)'](j,:)

j = 0 ;
j++ ;
j = 23 ;
i = 0 ; clear h ; clf ;
##for J = [1 1 2 2 ; 3 4 3 4]
for J = [1 1 ; 3 4]
   PWx = J(1) ; jMDL = J(2) ;
   printf("using %s\n", MDL{jMDL}) ;
   
   subplot(2, 1, ++i) ; hold on ;
   res = RES.(MDL{jMDL})(j) ;
##   res = pw(u = A.m, x = A.adata(:,j), MDL{jMDL}, Tfun, PWx, MODE, opts) ;

   h(2) = plot(A.t(1:end-PWx), res.y, "color", COL(jMDL,:), "linewidth", 1) ;
   h(3) = plot(A.t(1:end-PWx), A.X(1:end-PWx,:) * res.beta', "color", COL(jMDL,:), "linewidth", 3) ;
   h(1) = plot(A.t, A.adata(:,j), "k") ;
   datetick ;
   xlabel("year") ; ylabel("level  [cm]") ;
   set(gca, "box", "off") ;
   mdl = strrep(MDL{jMDL}, "Ys", "Y variant") ;
   title(sprintf("%s, %s", A.V{j}, mdl)) ;
##   hl(i) = legend(h, {"OBS" sprintf("PW (%s)", MDL{jMDL}),  sprintf("trend PW (%s)", MDL{jMDL})}, "box", "off", "location", "northwest") ;
##   pos = get(hl(i), "position") ; pos(2) += 0.03 ;
##   set(hl(i), "position", pos, "fontsize", 12) ;
   yt = 0.8 * ylim()(2) ;
   text(datenum(2005, 1, 1), yt, sprintf("{{\\itp = %.2f}}", res.pv)) ;

endfor

hgsave(sprintf("nc/Apx.%s.og", A.V{j})) ;
print(sprintf("nc/Apx.%s.svg", A.V{j})) ;

#{
## check divergence for PWx > 1
PWx = 2 ;
j = 2 ; xl = 0.92 ;
res = pw(u = A.m, x = A.data(:,j), mdl, Tfun, PWx, MODE, opts) ;
r = linspace(0, 0.99999, 350)' ;
r = [r res.ac(2) * ones(rows(r), 1)] ;
clf ;
fun = @(r) nthargout (4, @pw_fun, u, x, r, Tfun, MODE) ;
o = fun(r) ;
x = ARsim(A.ac1(:,j), A.av1(j), rows(A.m)) ;
fun = @(r) ((1 - r(:,1)).^2).^(-1/n) .* nthargout(4, @pw_fun, u, x, r, Tfun, MODE) ;
f = fun(r) ;
[ax h1 h2] = plotyy(r(:,1), o, r(:,1), f) ;
xlabel("{\\it\\phi_1}") ;
arrayfun(@(i) ylabel(ax(i), "HL error"), 1:2) ; 
legend({sprintf("%s", A.V{j}) sprintf("%s, surrogate", A.V{j})}, "box", "off") ; 
hgsave(sprintf("nc/regerr.%d.og", PWx)) ;
print(sprintf("nc/regerr.%d.svg", PWx)) ;

clf ; hold on ;
tx = ty = linspace (0.2, 0.9, 20)';
[xx, yy] = meshgrid (tx, ty);
f = @(p1,p2) nthargout(4, @pw_fun, u, x, [p1(:) p2(:)], Tfun, MODE) ;
tz = reshape(f(xx, yy), 20, 20) ;
mesh (xx, yy, tz); hold on
plot(res.ac(1), res.ac(2), f(res.ac(1), res.ac(2)), "x")
#}


## Hamed 09
n = 1000 ;
u = (1:n)' ;
e = arma_rnd(0.7, [], 1, 1000) ;
x = 2 * u + 100*e ;

[y beta ac] = H09 (u, x, Tfun, PWx = 1, MODE) ;

clf ; hold on
plot(u, x, u(1:end-1), [x(1:end-1)] * [ac]') ;
plot(u(1:end-1), [u(1:end-1)] * [beta]') ;


## plot iteration steps
Tfun = {@ols_est, @ThS}{1} ;
j = 2 ;
u = A.m ; x = A.data(:,j) ;
F = @(phi,beta) arrayfun(@(p,b) cost(p, b, u, x, Tfun, PWx, MODE), phi, beta) ;
opts = optimset("MaxIter", 1000, "TolX", 1e-8, "Display", "iter", "Meth", "yule") ;

iter = 0 ; phi = 0.9 * ones(1, PWx) ; beta = -1e-4 ; clear P ;
while ++iter < opts.MaxIter

   if 1
      Fphi = @(phi) F(phi,beta) ;
      wphi = fminsearch(Fphi, phi) ;
   else
      wphi = ARfit(x - u * beta, PWx, opts.Meth) ;
   endif
   Fbeta = @(beta) F(wphi,beta) ;
   wbeta = fminsearch(Fbeta, beta) ;

   dth = max(norm(wbeta-beta), norm(wphi-phi)) ;
   if strcmp(opts.Display, "iter")
      printf("%10d:\tdth = %.3g:\t(beta,phi) = (%.3g,%6.4f):\tF = %.5g\n", iter, dth, wbeta, wphi(1), Fbeta(wbeta)) ;
   endif

   phi = wphi ; beta = wbeta ;
   P(iter,:) = [phi beta] ;

   if dth < opts.TolX
      if strcmp(opts.Display, "final") || strcmp(opts.Display, "iter")
	 printf("final iteration: (dphi, dbeta) = (%.3g %.3g)\n", norm(wphi-phi), norm(wbeta-beta)) ;
      endif
      break ;
   endif

endwhile

phi = linspace(0.92, 0.999, 20) ;
beta = linspace(1e-5, 6e-5, 20) ;
[phi, beta] = meshgrid (phi, beta);
imagesc(F(phi, beta)) ;
imagesc(phi, beta, F(phi, beta)) ;
colorbar
contour(phi, beta, F(phi, beta)) ;

clf ;
P = P0 ;
scatter(P(:,1), P(:,2), [], (1:rows(P))') ;
hold on ;
P = P1 ;
scatter(P(:,1), P(:,2), [], (1:rows(P))') ;
