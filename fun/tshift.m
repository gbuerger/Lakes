## usage: [v, y] = tshift (u, x, p, MODE="R")
##
## apply time shift of order p
function [v, y] = tshift (u, x, p, MODE="R")

   X = cell2mat(arrayfun(@(l) shift(x, -l+1, 1), 1:p+1, "UniformOutput", false)) ;
   X = X(1:end-(p+1)+1,:) ;
   if strcmp(MODE, "R") ;
      X = flip(X, 2) ;
   endif

   v = [X(:,2:end) u(1:end-p)] ;
   y = X(:,1) ;

endfunction
