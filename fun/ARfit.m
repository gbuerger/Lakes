## usage: [ac av] = ARfit (x, p, meth = "yule")
##
## fit AR process of order p, using method meth
function [ac av] = ARfit (x, p, meth = "yule")

   x = center(x) ;
   
   switch meth
      case "yule"
	 [ac, av] = aryule(x, p) ;
	 ac = -ac(2:end) ;
      otherwise
         addpath ~/oct/nc/ARfit
         [~, ac, av] = arfit(x, p, p, "sbc", "zero") ;
   endswitch
   
endfunction
