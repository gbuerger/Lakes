## usage: y = onex (x)
##
## add 1's to x
function y = onex (x)
   y = [ones(size(x, 1), 1) x] ;
endfunction
