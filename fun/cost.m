## usage: F = cost (phi, beta, u, x, Tfun, PX = 1, MODE)
##
## cost function for phi, beta
##
function F = cost (phi, beta, u, x, Tfun, PX = 1, MODE)

   y = x ;

   X = cell2mat(arrayfun(@(l) shift(x, -l+1, 1), 1:PX+1, "UniformOutput", false)) ;
   X = X(1:end-(PX+1)+1,:) ;
   if strcmp(MODE, "R") ;
      X = flip(X, 2) ;
   endif

   u = [X(:,2:end) u(1:end-PX)] ;
   x = X(:,1) ;

   F = sumsq(x - u * [phi beta]') ;

endfunction
