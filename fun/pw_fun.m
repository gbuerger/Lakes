## usage: [y beta re sigma dw] = pw_fun (u, x, ac, Tfun, MODE)
##
## cost function for Tfun
function [y beta re sigma dw] = pw_fun (u, x, ac, Tfun, MODE)

   if (n = size(ac, 1)) > 1
      [y beta re sigma dw] = arrayfun(@(i) pw_fun (u, x, ac(i,:), Tfun, MODE), (1 : n)', "UniformOutput", false) ;
      y = cell2mat(y) ;
      beta = cell2mat(beta) ;
      re = cell2mat(re) ;
      sigma = cell2mat(sigma) ;
      dw = cell2mat(dw) ;
      return ;
   endif
   
   v = prw(u, ac, MODE) ;
   y = prw(x, ac, MODE) ;

   if numel(unique(v)) < 10
      y = re = NaN ;
      beta = nan(1, columns(u)) ;
      sigma = Inf ;
      return ;
   endif

   [beta sigma R] = Tfun(v, y) ;
   
   re = acor(R, 1) ;
   dw = durwat(R) ;
   
endfunction
