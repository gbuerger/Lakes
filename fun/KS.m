## usage: [y beta ac re dw] = KS (u, x, Tfun, PX = 1, MODE, meth = "")
##
## MK test for pre-whitening a la von Storch 1999
function [y beta ac re dw] = KS (u, x, Tfun, PX = 1, MODE, meth = "")

   ac = ARfit(x, PX, meth) ;

   v = u(1:end-PX) ;
   y = prw(x, ac, MODE) ;

   [beta sigma R] = Tfun(v, y) ;

   re = acor(R, PX) ;
   dw = durwat(R) ;

endfunction
