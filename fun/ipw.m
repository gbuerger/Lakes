## usage: [y beta ac] = ipw (u, x, r0 = 0.05, Tfun, MODE, opts)
##
## iterative pre-whitening (Wang and Swail 2001)
function [y beta ac] = ipw (u, x, r0 = 0.05, Tfun, MODE, opts)

   INIT = 0 ;
   
   ac = 0 ;
   if INIT == 0
      beta = 0 ;
   else
      beta = Tfun(u, x) ;
   endif
   
   for i = 1:opts.MaxIter

      r = acor(x - u * beta, 1) ;

      y = prw(x, r, MODE) / (1 - r) ;

      b = Tfun(u(1:end-1,:), y) ;
      
      if isequal(Tfun, @ThS)
	 bw = [median(x - u * beta, "omitnan") beta]' ;
      else
	 bw = [mean(x - u * beta, "omitnan") beta]' ;
      endif
      sigma = var(x - onex(u) * bw) ;

      dth = max(norm(beta-b), norm(ac-r)) ;
      if strcmp(opts.Display, "iter")
	 printf("%d:\tdth = %.3g:\tb = %.3g, r = %6.4f, sigma = %.5g\n", i, dth, b, r, sigma) ;
      endif
      
      if abs(r) < r0
	 if strcmp(opts.Display, "final")
	    printf("ac small enough: ac = %.2f\n", r) ;
	 endif
	 return ;
      endif
      if dth < opts.TolX
	 if strcmp(opts.Display, "final")
	    printf("diffs small enough: (dbeta, dac) = (%.3g %.3g)\n", norm(beta-b), norm(ac-r)) ;
	 endif
	 return ;
      endif

      beta = b ; ac = r ;

   endfor

   y = ac = beta = NaN ;
   
endfunction
