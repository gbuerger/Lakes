## usage: r = acor (x, lag=1)
##
## calculate autocorrelation of x
function r = acor (x, lag=1)

   r = xcov(x', lag, "coeff")(lag+2) ;
   
##   r = corr(x(1:end-lag), x(lag+1:end)) ;
   
endfunction
