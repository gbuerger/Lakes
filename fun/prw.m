## usage: y = prw (x, ac, MODE = "R")
##
## pre-whitening function
function y = prw (x, ac, MODE = "R")

   ac = [1 -ac(:)'] ;

   n = rows(x) ;
   p = length(ac) ;

   X = cell2mat(arrayfun(@(l) shift(x, -l+1, 1), 1:p, "UniformOutput", false)) ;
   X = X(1:end-p+1,:) ;

   if strcmp(MODE, "R") ;
      X = flip(X, 2) ;
   endif

   y = X * ac' ;
   
endfunction
