## usage: A = read_data (ifile)
##
## read data from ifile
function A = read_data (ifile)
   
   fid = fopen(ifile, "rt", "n", "iso-8859-1") ;
   s = textscan(fid, "%s", "delimiter", "\r\n"){:} ;
   fclose(fid) ;

   A.V = strsplit(s{1}, ";")(2:end) ;
   A.V = strrep(A.V, "\"", "") ;
   A.V = strrep(A.V, "_", " ") ;
   A.n = length(A.V) ;

   s = cellfun(@(c) strsplit(c, ";"), s(2:end), "UniformOutput", false) ;
   S = reshape(cell2mat(s), [], A.n+1) ;

   pkg load financial
   A.m = cell2mat(cellfun(@(c) (year(c, "yyyy-mm-dd") - 1) * 12 + month(c, "yyyy-mm-dd"), S(:,1), "UniformOutput", false)) ;
   A.t = cell2mat(cellfun(@(c) datenum(c, "yyyy-mm-dd"), S(:,1), "UniformOutput", false)) ;
   A.data = cellfun(@(c) str2num(c), S(:,2:end)) ;

endfunction
