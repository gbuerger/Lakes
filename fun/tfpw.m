## usage: [y beta ac] = tfpw (u, x, Tfun, scl = true, PX = 1, MODE, meth = "yule")
##
## MK test for trend-free pre-whitening a la Yue et al. 2002
## scl=true is used to respect the package "variance" mentioned in zyp. 
function [y beta ac] = tfpw (u, x, Tfun, scl = true, PX = 1, MODE, meth = "yule")
   
   beta = Tfun(u, x) ;

   if isequal(Tfun, @ThS)
      alpha = median(x - u * beta, "omitnan") ;
   else
      alpha = mean(x - u * beta, "omitnan") ;
   endif

   t = alpha + u * beta ;

   x = x - t ;
   
   ac = ARfit(x, PX, meth) ;

   y = prw(x, ac, MODE) ;

   if PX < 2 && scl
      y = y / (1 - ac(1)) ;
   endif

   y = y + t(1:end-PX,:) ;

endfunction
