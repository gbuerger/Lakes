## usage: [beta sigma R dw] = ols_est (u, x)
##
## estimate beta
function [beta sigma R dw] = ols_est (u, x)

   N = size(x) ;

   if N(2) > 1
      [beta sigma R] = arrayfun(@(i) ols_est(u(:,i), x(:,i)), 1 : N(2), "UniformOutput", false) ;
      beta = cell2mat(beta) ;
      sigma = cell2mat(sigma) ;
      R = cell2mat(R) ;
      if nargout < 4 return ; endif
      dw = cell2mat(dw) ;
      return ;
   endif

   u = onex(u) ;
   
   beta = ols(x, u) ;

   F = u * beta ;
   beta = beta(2:end) ;

   if nargout < 2 return ; endif

   R = x - F ;
   sigma = sumsq(R) ;
##   sigma = sumsq(R) ./ sumsq(F) ; # goodness of fit

   if nargout < 4 return ; endif

   dw = durwat(R) ;

endfunction
