## usage: rand_init (rfile = "rand_init.ot", force = false)
##
## initialize rng
function rand_init (rfile = "rand_init.ot", force = false)

   if exist(rfile = rfile, "file") && ~force
      load(rfile) ;
      randn ("state", rnd) ;
   else
      rnd = randn ("state") ;
      save(rfile, "-text", "rnd") ;
   endif

endfunction
