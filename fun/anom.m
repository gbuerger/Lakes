## usage: y = anom (m, x)
##
## monthly anomalies of x using months m
function y = anom (m, x)

   y = x ;
   for mon = (1 : 12) - 1
      I = rem(m, 12) == mon ;
      y(I,:) = y(I,:) - mean(y(I,:)) ;
   endfor
   
endfunction
