## usage: [beta alpha] = gls_est (u, x, rho)
##
## do GLS
function [beta sigma R] = gls_est (u, x, rho)

   n = rows(x) ;
   O = diag (rho * ones(n-1, 1), -1) + diag (rho * ones(n-1, 1), 1) + diag (ones(n, 1)) ;

   xm = mean(x, "omitnan") ; um = mean(u, "omitnan") ;
   beta = real(gls(x - xm, u - um, O)) ;
   alpha = xm - um * beta ;

   R = x - (alpha + u * beta) ;
   sigma = sumsq(R) ;

endfunction
