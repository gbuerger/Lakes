## usage: res = pw (u, x, mdl="CO", Tfun=@ThS, PX = 1, MODE = "R", varargin)
##
## apply PW
function res = pw (u, x, mdl="CO", Tfun=@ThS, PX = 1, MODE = "R", varargin)
   
   opts = optimset(varargin{:}) ;
   if isempty(opts.MaxIter) opts.MaxIter = 1000 ; endif
   if ~isfield(opts, "Meth") opts.Meth = "yule" ; endif
   if isempty(opts.TolX) opts.TolX = 1e-6 ; endif

   switch mdl
      case "KS"	 ## von Storch
	 [y beta ac re dw] = KS (u, x, Tfun, PX, MODE, opts.Meth) ;
      case "CO"	 ## Cochrane and Orcutt
	 [y beta ac re n dw] = pw_loop(u, x, Tfun, :, PX, MODE, opts) ;
      case "HL"	 ## Hildreth and Lu
	 f = @(ac) nthargout(4, @pw_fun, u, x, ac, Tfun, MODE) ;
	 if PX < 2 && 0
	    ac = grids(f, [0 1], 20, :, opts) ;
	 else
	    [ac, v, nev] = fminsearch (f, 0.5*ones(1, PX), opts) ;
	 endif
	 [y beta re ~, dw] = pw_fun(u, x, ac, Tfun, MODE) ;
      case "WS"	 ## Wang and Swail
	 if PX > 1 error("WS not adapted to PX > 1") ; endif
	 [y beta ac] = ipw(u, x, :, Tfun, PX, MODE, opts) ;
	 [~, ~, re ~, dw] = pw_fun (u, x, ac, Tfun, MODE) ;
      case "Y"   ## Yue et al.
	 [y beta ac] = tfpw(u, x, Tfun, false, PX, MODE) ;
	 [~, ~, re ~, dw] = pw_fun (u, x, ac, Tfun, MODE) ;
      case "Ys"   ## Yue et al.
	 [y beta ac] = tfpw(u, x, Tfun, true, PX, MODE) ;
	 [~, ~, re ~, dw] = pw_fun (u, x, ac, Tfun, MODE) ;
      case "H09"   ## Hamed 2009
	 [y beta ac] = H09(u, x, Tfun, PX, MODE) ;
	 [~, ~, re ~, dw] = pw_fun (u, x, ac, Tfun, MODE) ;
      case "ML"	 ## Judge et al.
	 f = @(ac) (1 - norm(ac).^2).^(-1/rows(u)) .* nthargout(4, @pw_fun, u, x, ac, Tfun, MODE) ;
	 if PX < 2 && 0
	    ac = grids(f, [0 1], 20, :, opts) ;
	 else
	    [ac, v, nev] = fminsearch (f, 0.5*ones(1, PX), opts) ;
	 endif
	 [y beta re ~, dw] = pw_fun(u, x, ac, Tfun, MODE) ;
      otherwise	 ## no pre-whitening
	 ac = ARfit(x, PX, opts.Meth) ;
	 y = x ;
	 [beta sigma R ~, dw] = Tfun(u, y) ;
	 re = acor(R, 1) ;
   endswitch

   ## check AR(p)-error for epsilon wrt. phi
   uw = prw(u, ac, MODE) ;
   xw = prw(x, ac, MODE) ;

   if isequal(Tfun, @ThS)
      x0 = median(x - u * beta) ;
      xw0 = median(xw - uw * beta) ;
   else
      x0 = mean(x - u * beta) ;
      xw0 = mean(xw - uw * beta) ;
   endif

   e = xw - onex(uw) * [xw0 beta]' ;
   dw = durwat(e) ;
   
   beta = [x0 beta] ;

   ct = cor_test((1:length(y))', y, "<>", "kendall") ;
   tau = ct.stat ;
   pv = ct.pval ;
   
   res = struct("beta", beta, "ac", ac, "tau", tau, "pv", pv, "re", re, "dw", dw, "y", y) ;
   
endfunction


## usage: [y beta ac re n dw] = pw_loop (u, x, Tfun, a0=0.05, PX, MODE, opts)
##
## Cochrane-Orcutt PW loop
function [y beta ac re n dw] = pw_loop (u, x, Tfun, ac0=0.05, PX, MODE, opts)

   INIT = 0 ;
   
   n = re = ac = dw = 0 ;

   if INIT == 0
      beta = 0 ;
   else
      beta = Tfun(u, x) ;
   endif

   while n++ < opts.MaxIter

      ## estimate ac
      wac = ARfit(x - u * beta, PX, opts.Meth) ;

      ## estimate beta
      if 1
	 [y b re ~, dw] = pw_fun (u, x, wac, Tfun, MODE) ;
      else
	 if PX > 1, error("gls_est not adapted to PX > 1") ; endif
	 b = gls_est(u, x, wac) ;
	 y = x ;
      endif
      
      if isequal(Tfun, @ThS)
	 bw = [median(x - u * beta, "omitnan") beta]' ;
      else
	 bw = [mean(x - u * beta, "omitnan") beta]' ;
      endif
      sigma = var(x - onex(u) * bw) ;
      
      dth = max(norm(beta-b), norm(ac-wac)) ;
      if strcmp(opts.Display, "iter")
	 printf("%d:\tdth = %.3g:\tb = %.3g, wac = %6.4f, sigma = %.5g\n", n, dth, b, wac(1), sigma) ;
      endif

      if abs(wac) < ac0
	 if strcmp(opts.Display, "final")
	    printf("a small enough: a = %.2f\n", wac) ;
	 endif
	 return ;
      endif
      if dth < opts.TolX
	 if strcmp(opts.Display, "final")
	    printf("diffs small enough: (dbeta, da) = (%.3g %.3g)\n", norm(beta-b), norm(ac-wac)) ;
	 endif
	 return ;
      endif
      
      ac = wac ; beta = b ;

   endwhile

   y = NaN ;
   
endfunction
