## usage: [beta sigma R dw] = ThS (u, x)
##
## trend function
function [beta sigma R dw] = ThS (u, x)

   if (n = size(x, 2)) > 1
      [beta sigma R] = arrayfun(@(i) ThS(u(:,i), x(:,i)), 1 : n, "UniformOutput", false) ;
      beta = cell2mat(beta) ;
      sigma = cell2mat(sigma) ;
      R = cell2mat(R) ;
      if nargout < 4 return ; endif
      dw = cell2mat(dw) ;
      return ;
   endif
   
   [beta alpha] = TheilSen ([u x]) ;

   F = alpha + u * beta ;
   R = x - F ;
   
   sigma = sumsq(R) ;
##   sigma = sumsq(R) ./ sumsq(F) ; # goodness of fit

   if nargout < 4 return ; endif

   dw = durwat(R) ;

endfunction
