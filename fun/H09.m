## usage: [y beta ac] = H09 (u, x, Tfun, PX = 1, MODE)
##
## simultaneous beta/rho estimate from Hamed 2009
##
function [y beta ac] = H09 (u, x, Tfun, PX = 1, MODE)

   y = x ;

   [u x] = tshift(u, x, PX, MODE) ;
   
   beta = Tfun(u, x) ;
   [ac beta] = deal(beta(1:end-1), beta(end)) ;
   ac = ac' ;

endfunction
