## usage: res = ARsim (a, v, n)
##
## simulate AR process
function res = ARsim (a, v, n)

   res = arma_rnd(a, [], v, n) ;
   
endfunction
