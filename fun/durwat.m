## usage: dw = durwat (r)
##
## calculate Durbin-Watson statistic of residuals r
function dw = durwat (r)

   N = size(r) ;
   I = repmat({":"}, 1, length(N)) ;
   I{1} = 1 : N(1)-1 ;

   rs = shift(r, -1, 1) ;

   dw = sumsq(rs(I{:})) ./ sumsq(r(I{:})) ;

endfunction
