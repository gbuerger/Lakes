## usage: [y, f] = grids (fun, x, n, iter = 0, opts)
##
## grid search for interval x
function [y, f] = grids (fun, x, n, iter = 0, opts)

   iter++ ;
   y = x ;
   
   x = linspace(x(1), x(2), n) ;
   fx = arrayfun(@(u) fun(u), x) ;
   
   [f j] = min(fx) ;
   if strcmp(opts.Display, "iter")
      printf("%5d:\t%12.5e\t%12.5e\t-->\t%10.5e\n", iter, y, f) ;
   endif
   
   i1 = max(j - 1, 1) ;
   i2 = min(j + 1, n) ;
   y = x([i1 i2]) ;

   if iter < 10
      [y f] = grids (fun, y, n, iter, opts) ;      
   else
      y = mean(y) ;
   endif
   
endfunction
